/* 
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :::::::                                             ::::::::
   :::::::    JQUERY VALIDATION FOR REGISTER PAGE      ::::::::
   :::::::                                             ::::::::
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

*/


$(document).ready(function() {

    // Add validator method 
    addValidator();

    $("#register_form").validate({
        rules: {
            first_name: {
                required: true,
                nameRegex: true
            },
            last_name: {
                required: true,
                nameRegex: true
            },
            email: {
                required: true,
                email: true
            },

            password: "required",

            cpassword: {
                required: true,
                equalTo: "#password",
                minlength: 8
            },

            mobile_number: {
                required: true,
                rangelength: [10, 10]
            }

        },

        messages: {
            first_name: {
                required: "First Name can not be empty.",
                nameRegex: "First Name can not contain any special character or digit."
            },
            last_name: {
                required: "Last Name can not be empty.",
                nameRegex: "Last Name can not contain any special character or digit."
            },

            mobile_number: {
                required: "Mobile number required.",
                rangelength: "Invalid mobile number.",
            },
            email: {
                required: "Email can not be empty.",
                email: "Invalid email format."
            },

            password: "Password can not be empty.",

            cpassword: {
                required: "Confirm your password",
                equalTo: "Passwords should be same.",
                minlength: "Passwords strength is too low, minimum 8 letters are allowed."
            },

        },

        // if validation are done.
        submitHandler: function() {

            // Registering the user.
            $.ajax({
                url: "http://www.youthclubapi.com/register",
                type: "POST",
                data: JSON.stringify($("#register_form").serializeToJSON()),
                dataType: "json",

                success: function(result) {

                    // if user register successfully
                    $("#register_status").html(result.message);
                    $("#register_status").css({
                        "text-align": "center",
                        "margin-bottom": "2%",
                        "background-color": "lime",
                        "font-weight": "bold"
                    });

                },

                error: function(error) {

                    // If user registration failed or user exists
                    $("#register_status").html(error.responseJSON.message);
                    $("#register_status").css({
                        "color": "red",
                        "text-align": "center",
                        "background-color": "orange",
                        "font-weight": "bold"
                    });
                }
            });
        }
    });
});

function addValidator() {

    // validate name character custom method
    jQuery.validator.addMethod("nameRegex", function(value, element) {
        return this.optional(element) || /^[a-z\ \s]+$/i.test(value);
    }, "Name must contain only letters & space");
}