/* 
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :::::::                                             ::::::::
   :::::::  JQUERY VALIDATION FOR LOGIN PAGE           ::::::::
   :::::::                                             ::::::::
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

*/

$(document).ready(function() {

    // Getting value from cookies
    $("#email").attr("value", getCookie("email"));
    $("#password").attr("value", getCookie("password"));
    if (getCookie("email") != "") {
        $("#remember").attr('checked', true);
    }


    $("#login").validate({
        rules: {
            email: {
                required: true,
                email: true
            },

            password: "required"
        },

        messages: {
            email: {
                required: "Email can not be empty.",
                email: "Invalid email format."
            },

            password: "Password can not be empty."
        },

        // if validation are done.
        submitHandler: function() {


            $.ajax({
                type: "POST",
                url: "http://www.youthclubapi.com/login",
                data: JSON.stringify($("#login").serializeToJSON()),
                dataType: "json",

                // If logged in successfully
                success: function(result) {

                    // Storing email and password in cookie if user clicked on remember
                    if ($("#remember").is(":checked")) {
                        setCookie("email", $("#email").val(), 30);
                        setCookie("password", $("#password").val(), 30);
                    } else {
                        setCookie("email", "", -1);
                        setCookie("password", "", -1);
                    }

                    //Storing jwt token in cookie storage
                    setCookie("token", result.jwt, 0.1);

                    // Forwarding to welcome page
                    window.location = "/welcome";

                },

                // if user is not exist or credential mismatched.
                error: function(error) {

                    $("#err").html(error.responseJSON.message);
                }

            });
        }
    });
});