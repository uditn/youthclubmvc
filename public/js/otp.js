/* 
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :::::::                                             ::::::::
   :::::::   API IMPLEMENTATION FOR OTP VERIFICATION   ::::::::
   :::::::                                             ::::::::
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

*/


// Sending OTP
function sendOTP() {
    var mobile_number = $('input[name="mobile_number"]').val();

    $.ajax({
        type: "POST",
        url: "http://www.youthclubmvc.com/otp/sendotp",
        data: {
            "mob": mobile_number,
        },
        dataType: "json",
        success: function(result) {

            var response = $.parseJSON(result);

            if (response.return == true) {
                $('#mobile_number_err').html("Otp has been send.");
                $('#mobile_number_err').css('color', 'green');
                $("#send_otp").removeClass("btn-warning").addClass("btn-success");
                $("#send_otp").attr('disabled', 'disabled');
                $("#otp").removeAttr('disabled');
                $("#otp_btn").removeAttr('disabled');
            } else if (response.return == false) {
                $('#mobile_number_err').html("Invalid number.");
                $('#mobile_number_err').css('color', 'red');
            }
        }
    });
}

// Varifying OTP
function verifyOTP() {

    var otp = $('input[name="otp"]').val();

    $.ajax({
        type: "POST",
        url: "http://www.youthclubmvc.com/otp/verifyotp",
        data: {
            "otp": otp,
        },
        dataType: "json",
        success: function(result) {
            if (result.hasOwnProperty('status') && result.status == "true") {
                $("#otp").attr('disabled', 'disabled');
                $("#otp_btn").attr('value', 'Verified');
                $("#otp_btn").removeClass("btn-warning").addClass("btn-success");
                $("#otp_btn").attr('disabled', 'disabled');
                $("#reg_btn").removeAttr('disabled');
                $('#otp_err').html("");

            } else if (result.hasOwnProperty('status') && result.status == "false") {
                $('#otp_err').html("Invalid otp. Try again.");
                $('#otp_err').css('color', 'red');
            }

        }
    });
}