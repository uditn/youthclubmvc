/* 
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :::::::                                             ::::::::
   :::::::    JQUERY VALIDATION FOR PROFILE PAGE      ::::::::
   :::::::                                             ::::::::
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

*/

$(document).ready(function() {

    // showing profile's data
    showProfile();

    // Add validator method 
    addValidator();

    // validating requested data
    $("#update-profile-form").validate({
        rules: {
            first_name: {
                required: true,
                nameRegex: true
            },
            last_name: {
                required: true,
                nameRegex: true
            },
            mobile_number: {
                required: true,
                rangelength: [10, 10]

            },
            'skills[]': {
                required: true,
                minlength: 2
            },
            age: {
                limit: [20, 30]
            },
            resume: {
                extension: "pdf|doc|docx",
                filesize: 1024000,
            },
            avatar: {
                extension: "png|jpg|gif",
                filesize: 512000
            }
        },

        messages: {

            first_name: {
                required: "First Name can not be empty.",
                nameRegex: "First Name can not contain any special character or digit."
            },
            last_name: {
                required: "Last Name can not be empty.",
                nameRegex: "Last Name can not contain any special character or digit."
            },

            mobile_number: {
                required: "Mobile number required.",
                rangelength: "Invalid mobile number.",
            },
            'skills[]': {
                required: "Select your skills.",
                minlength: "You must check at least 2 skills."
            },
            age: {
                limit: "Age limit should 20-30."
            },
            resume: {
                extension: "Please select only pdf and doc files.",
                filesize: "Please upload file within 1024KB"
            },
            avatar: {
                extension: "Please select only jpg, png, gif files.",
                filesize: "Please upload file within 512KB"
            },

        },
        // Placing error msg of checkbox after all checkbox
        errorPlacement: function(label, element) {
            if (element.attr("name") === "skills[]") {
                // this would append the label after all your checkboxes/labels 
                //(so the error-label will be the last element in <div class="controls"> )

                element.parent().parent().parent().append(label);
            } else {
                // standard behaviour
                label.insertAfter(element);
            }
        },
        submitHandler: function(form) {

            // Adding form data
            data = new FormData(form);

            // Updating profile data
            updateProfile(data);
        }
    });
});


// Getting user's data and showing profile page
function showProfile() {

    $.ajax({
        type: "POST",
        url: "http://www.youthclubapi.com/verifytoken",
        data: JSON.stringify({
            "token": getCookie("token")
        }),
        dataType: "json",

        // If token authorization succeed
        success: function(result) {

            // Showing users profile information
            $("[name=first_name]").attr("value", result.data.first_name);
            $("[name=last_name]").attr("value", result.data.last_name);
            $("[name=email]").attr("value", result.data.email);
            $("[name=mobile_number]").attr("value", result.data.mobile_number);
            $("[name=sex]").attr("value", result.data.gender);
            $("[name=default_state]").attr("value", result.data.state);
            $("[name=age]").attr("value", result.data.age);

            $("#" + result.data.gender.toLowerCase()).attr("checked", "checked");

            if (result.data.state != "") {
                $("#default_state").html(result.data.state);
            }

            // Adding skills to profile page
            if (result.data.skills != "") {
                result.data.skills.forEach(skill => {
                    $('<input />', {
                        type: 'checkbox',
                        disabled: 'disabled',
                        checked: 'checked'
                    }).appendTo("#form-check-label");

                    $('<label />', { 'for': skill, text: skill }).appendTo("#form-check-label").css({
                        "margin-left": "5px",
                        "padding-right": "20px"
                    });
                    $("#" + skill).attr("checked", "checked");
                });
            }


            // Showing profile picture
            var path = "";
            if (result.data.avatar_path == null) {
                if (result.data.gender == "Male") {
                    path = "public/images/avatar_m.png";
                } else if (result.data.gender == "Female") {
                    path = "public/images/avatar_f.png";
                } else {
                    path = "public/images/default_avatar.png";
                }
            } else {
                path = "http://www.youthclubapi.com/app/users/" + result.data.avatar_path;

            }
            $("#avatar_img").attr("src", path);
            $("#profile-usertitle-name").html(result.data.first_name + " " + result.data.last_name);

        },

        // if token authorization failed
        error: function(error) {
            //  Invalid token or token expired, login again
            window.location = "/login/logout";

        }
    });
}

// Updating user's data in database
function updateProfile(data) {

    // Adding token to field
    data.append("token", getCookie("token"));

    $.ajax({
        type: "POST",
        processData: false,
        contentType: false,
        cache: false,
        enctype: 'multipart/form-data',
        url: "http://www.youthclubapi.com/updateprofile",
        data: data,
        dataType: "json",

        // If profile successfully updated
        success: function(result) {

            // if updataion succeed show success message
            $("#profile-update-status").html(result.message);
            $("#profile-update-status").css({
                "color": "black",
                "text-align": "center",
                "margin-bottom": "2%",
                "background-color": "lime",
                "font-weight": "bold"
            });

            //Storing jwt token in cookie storage
            setCookie("token", result.jwt, 0.1);

            // showing updated profile's data
            showProfile();

            // forwording to profile tab
            $('a[href="#home"]').click();
        },
        error: function(error) {

            console.log(error);

            // if updataion failed show error message
            $("#profile-update-status").html(error.responseJSON.message);
            $("#profile-update-status").css({
                "color": "red",
                "text-align": "center",
                "background-color": "orange",
                "font-weight": "bold"
            });
        }

    });
}

// Adding extra validator method
function addValidator() {

    //validate file size custom  method.
    $.validator.addMethod('filesize', function(value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than {0}');

    //validate file extension custom  method.
    jQuery.validator.addMethod("extension", function(value, element, param) {
        param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    }, "Please enter a value with a valid extension.");

    //validate age limit custom  method.
    jQuery.validator.addMethod("limit", function(value, element, param) {
        return value > param[0] && value < param[1]
    }, "Age limit does not satisfy.");

    // validate name character custom method
    jQuery.validator.addMethod("nameRegex", function(value, element) {
        return this.optional(element) || /^[a-z\ \s]+$/i.test(value);
    }, "Name must contain only letters & space");

}

// If user upload image
function showUploadImage() {
    $("#avatar_img").attr("src", URL.createObjectURL(event.target.files[0]));
}