/* 
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :::::::                                             ::::::::
   :::::::  JQUERY VALIDATION FOR CHANGE-PASSWORD PAGE ::::::::
   :::::::                                             ::::::::
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

*/

$(document).ready(function() {

    $("#changepassword").validate({
        rules: {
            opassword: "required",

            npassword: {
                required: true,
            },

            cpassword: {
                required: true,
                equalTo: "#npassword",
                minlength: 8
            }
        },

        messages: {
            opassword: "Please enter old password.",
            npassword: {
                required: "Please enter new password",
            },
            cpassword: {
                required: "Confirm your password",
                equalTo: "Confirm password should be same."
            }
        },

        submitHandler: function() {

            $.ajax({
                type: "POST",
                url: "http://www.youthclubapi.com/changepassword",
                data: JSON.stringify({
                    "opassword": $("#opassword").val(),
                    "npassword": $("#npassword").val(),
                    "token": getCookie("token")
                }),
                dataType: "json",

                // If password successfully updated
                success: function(result) {

                    $("#change-password-status").html(result.message);
                    $("#change-password-status").css({
                        "color": "black",
                        "text-align": "center",
                        "margin-bottom": "2%",
                        "background-color": "lime",
                        "font-weight": "bold"
                    });

                    //Storing jwt token and new password in cookie storage
                    setCookie("token", result.jwt, 0.1);
                    setCookie("password", $("#npassword").val(), 30);
                },
                error: function(error) {
                    $("#change-password-status").html(error.responseJSON.message);
                    $("#change-password-status").css({
                        "color": "red",
                        "text-align": "center",
                        "background-color": "orange",
                        "font-weight": "bold"
                    });
                }

            });
        }
    });
});