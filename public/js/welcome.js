/* 
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   :::::::                                             ::::::::
   :::::::    JQUERY VALIDATION FOR WELCOME PAGE      ::::::::
   :::::::                                             ::::::::
   ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

*/


// Getting user's data and showing welcome message
$(document).ready(function() {
    $.ajax({
        type: "POST",
        url: "http://www.youthclubapi.com/verifytoken",
        data: JSON.stringify({
            "token": getCookie("token")
        }),
        dataType: "json",

        // If token authorization succeed
        success: function(result) {

            $("#welcome_message").html("Hi     " + result.data.first_name);

        },

        // if token authorization failed
        error: function(error) {
            //  Invalid token or token expired, login again
            window.location = "/login/logout";

        }


    });
});