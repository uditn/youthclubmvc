<?php

class Main extends Controller
{

    /*
     * http://localhost/
     */
    function Index()
    {

        if (!isset($_SESSION['login'])) {

            header('Location: /dashboard');
        } else {

            header('Location: /welcome');
        }
    }
}

?>