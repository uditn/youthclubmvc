<?php

class Profile extends Controller
{

    /*
     * http://localhost/profile
     */
    function Index()
    {

        if (!isset($_SESSION['login'])) {

            $_SESSION["login_err"] = "You need to be login first.";
            header("Location: /login");
        } else {

            $this->view('profile', array("title" => "Home | Profile", "set_header" => "profile"));
        }
    }

   
}
