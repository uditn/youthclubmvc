<?php

class Login extends Controller
{

    /*
     * http://localhost/login
     */
    function Index()
    {

        if (!isset($_SESSION['login'])) {

            $this->view('login', array("title" => "Login Here", "set_header" => "login"));
        } else {

            header('Location: /welcome');
        }
    }

    /*
     * http://localhost/login/logout
     */
    function Logout()
    {

        unset($_SESSION["login"]);
        session_unset();
        setcookie("token", null, -1, "/");
        header('Location: /');
    }
}
