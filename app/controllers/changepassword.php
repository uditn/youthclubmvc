<?php

class ChangePassword extends Controller
{

    /*
     * http://localhost/changepassword
     */
    function Index()
    {

        if (!isset($_SESSION['login'])) {

            $_SESSION["login_err"] = "You need to be login first.";
            header("Location: /login");
        } else {

            $this->view('changepassword', array("title" => "Update Password", "set_header" => "changepassword"));
        }
    }
}

?>