<?php

class Dashboard extends Controller
{

    /*
     * http://localhost/dashboard
     */
    function Index()
    {

        if (!isset($_SESSION['login'])) {

            $this->view('dashboard/index', array("title" => "Youthclub", "set_header" => "dashboard"));
        } else {

            header('Location: /welcome');
        }
    }
}

?>
