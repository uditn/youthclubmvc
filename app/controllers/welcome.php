<?php

class Welcome extends Controller
{

    /*
     * http://localhost/welcome
     */
    function Index()
    {

        if (!isset($_SESSION['login'])) {

            $_SESSION["login_err"] = "You need to be login first.";
            header("Location: /login");
        } else {

            $this->view('welcome', array("title" => "YouthClub", "set_header" => "welcome"));
        }
    }

}

?>