<?php

class About extends Controller
{

    /*
     * http://localhost/about
     */
    function Index()
    {
        $data["title"] = "About us";
        
        if (!isset($_SESSION['login'])) {
            $data["set_header"] = "nonauth_about";
        } else {
            $data["set_header"] = "auth_about";
        }

        $this->view('about', $data);
    }
}
