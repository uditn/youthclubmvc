<?php

class  Otp extends Controller
{
    function Index()
    {
    }

    // Sending otp
    function sendOtp()
    {
        $number = urlencode($_POST["mob"]);
        $otp = rand(100000, 999999);
        $variables_values = urlencode($otp);
        $curl = curl_init();
        $url = "https://www.fast2sms.com/dev/bulk?authorization=".$this->config["fast2sms"]["API_KEY"]."&sender_id=".$this->config["fast2sms"]["SENDER"]."&language=".$this->config["fast2sms"]["LANGUAGE"]."&route=".$this->config["fast2sms"]["ROUTE"]."&message=".$this->config["fast2sms"]["MESSAGE"]."&variables=".$this->config["fast2sms"]["VARIABLES"]."&variables_values=$variables_values&numbers=$number";

        curl_setopt_array($curl, array(

            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);


        if ($err) {
            echo json_encode($err);
        } else {
            $_SESSION['otp'] =  $otp;
            echo json_encode($response);
        }
    }

    // Verifying otp
    function verifyOtp()
    {
        $result = [];

        $otp = $_POST['otp'];

        if ($otp == $_SESSION['otp']) {

            unset($_SESSION['otp']);
            $result["status"] = "true";
        } else {

            $result["status"] = "false";
        }

        echo json_encode($result);
    }
}
