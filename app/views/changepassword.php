<!-- Body part -->

<body>

    <div class="container form-container">
        <div class="row">
            <div class="col-md-12">
                <form class="form" id="changepassword">

                    <div class="col-md-12" id="change-password-status"></div>

                    <h3 class="text-center text-dark">Update Your Password</h3><br>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="old_password" class="text-dark">Old Password:</label>
                            <input type="password" name="opassword" id="opassword" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="newpassword" class="text-dark">New Password:</label>
                            <input type="password" name="npassword" id="npassword" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="confirmpassword" class="text-dark">Confirm New Password:</label>
                            <input type="password" name="cpassword" id="cpassword" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" id="change_password_btn_div">
                        <input type="submit" id="update" value="Update" class="btn btn-dark btn-md">
                    </div>

                </form>
            </div>
        </div>
    </div>

    <!-- Adding js for validation and otp verification -->
    <script src="public/js/changepassword.js"></script>

</body>