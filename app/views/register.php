<!-- Body part -->

<body>

    <div class="container form-container">
        <div class="row">
            <div class="col-md-12">
                <form id="register_form" class="form">

                    <div class="col-md-12" id="register_status"></div>
                    
                    <h3 class="text-center text-dark">Register Here</h3><br>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="first_name" class="text-dark">First Name:</label>
                            <input type="text" name="first_name" id="first_name" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="last_name" class="text-dark">Last Name:</label>
                            <input type="text" name="last_name" id="last_name" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="username" class="text-dark"> Username:</label>
                            <input type="text" name="email" id="email" class="form-control">
                        </div>
                        <div id="mobile_number_div" class="col-md-2">
                            <label for="mobile_number" class="text-dark"> Mobile No:</label>
                            <input type="number" name="mobile_number" id="mobile_number" class="form-control" id="mobile_number" >
                            <div id="mobile_number_err"></div>
                        </div>

                        <div class="col-md-1" id="send-otp-btn-div">
                            <input type="button" class="btn btn-warning btn-md" onclick="sendOTP()" value="Send OTP" id="send_otp">
                        </div>
                        <div id="varify_otp_div" class="col-md-2">
                            <input type="number" name="otp" class="form-control" id="otp" disabled>
                            <div id="otp_err"></div>
                        </div>
                        <div id="varify_otp_btn_div" class="col-md-1">
                            <input type="button" class="btn btn-warning btn-md" onclick="verifyOTP()" value="Varify OTP" id="otp_btn" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="password" class="text-dark">Password:</label>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="confirmpassword" class="text-dark">Confirm Password:</label>
                            <input type="password" name="cpassword" id="cpassword" class="form-control">
                        </div>
                    </div>
                    <div class="form-group col-md-12" id="register_btn_div">
                        <input type="submit" id="reg_btn"  value="Register" class="btn btn-dark btn-md" disabled>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Adding js for validation and otp verification -->
    <script src="public/js/register.js"></script>
    <script src="public/js/otp.js"></script>

</body>