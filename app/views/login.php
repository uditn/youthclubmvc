<!-- Body part -->

<body>
    <div class="container form-container">
        <div class="row">
            <div class="col-md-12">
                <form class="form" id="login">
                    <div id="err">
                        <?php if(isset($_SESSION["login_err"])){
                            echo $_SESSION["login_err"];
                            unset($_SESSION["login_err"]);
                        }?>
                    </div>
                    <h3 class="text-center text-dark">Login</h3>
                    <div class="form-group">
                        <label for="username" class="text-dark">Username:</label>
                        <input type="text" name="email" id="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="password" class="text-dark">Password:</label>
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                    <div class="field-group">
                        <div><input type="checkbox" name="remember" id="remember">
                            <label class="text-dark" for="remember-me">Remember me</label>
                        </div>
                        <div class="form-group" id="login_btn_div">
                            <input type="submit" value="Login" class="btn btn-dark btn-md">
                        </div>
                        <div id="register-link" class="text-right">
                            <a href="register" class="text-warning">Register here</a>
                        </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Adding js for validation and login operation -->
    <script src="public/js/login.js"></script>

</body>