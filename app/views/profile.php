<!-- Body part -->

<body>
    <div class="container profile_container">

    <!--  Profile status -->
    <div id="profile-update-status"></div>

        <div class="col-md-12">
            <!-- Profile Image -->
            <div class="col-md-4">
                <div class="portlet light profile-sidebar-portlet bordered">
                    <div class="profile-userpic">
                        <img id="avatar_img" class="img-responsive img-thumbnail" alt="Profile Picture">
                    </div>
                    <div class="profile-usertitle">
                        <div id="profile-usertitle-name"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="portlet light bordered">
                    <!-- Taba Info -->
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase"><b>Your Info</b></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li id="profile-tab" class="active"><a href="#home" data-toggle="tab">Profile</a></li>
                                <li id="update-tab"><a href="#update" data-toggle="tab">Update</a></li>
                            </ul>

                            <!-- Profile Tab -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home">
                                    <form id="show-prfile-form"><br>
                                        <div class="form-group">
                                            <label for="inputFirstName">First Name</label>
                                            <input type="text" name="first_name" class="form-control" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputLastName">Last Name</label>
                                            <input type="text" class="form-control" name="last_name" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control" name="email" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputMobileNumber">Mobile No.</label>
                                            <input type="number" class="form-control" name="mobile_number" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputGender">Gender</label>
                                            <input type="text" class="form-control" name="sex" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputState">State</label><br>
                                            <input type="text" class="form-control" name="default_state" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputSkills">Skills</label>
                                            <div class="form-check-inline">
                                               <label id='form-check-label'>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputAge">Age</label>
                                            <input type="number" class="form-control" name="age" readonly>
                                        </div>
                                    </form>
                                </div>

                                <!-- Update tab -->

                                <div class="tab-pane" id="update">
                                    <form id="update-profile-form"><br>
                                        <div class="form-group">
                                            <label for="inputFirstName">First Name</label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="inputLastName">Last Name</label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control" id="email" name="email" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputMobileNumber">Mobile No.</label>
                                            <input type="number" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputGender">Gender</label>
                                            <div class="form-check-inline">
                                                <label class="form-check-label" for="male">
                                                    <input type="radio" class="form-check-input" id="male" name="gender" value="Male" checked > Male
                                                </label>
                                                <label class="form-check-label" for="female">
                                                    <input type="radio" class="form-check-input" id="female" name="gender" value="Female" > Female
                                                </label>
                                                <label class="form-check-label" for="other">
                                                    <input type="radio" class="form-check-input" id="other" name="gender" value="Other" > Other
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputState">State</label><br>
                                            <select class="browser-default custom-select" id="state" name="state">
                                                <option name="default_state" id="default_state">------------Select State------------</option>
                                                <?php
                                                    $states = array("Assam", "Bihar", "Delhi", "Goa", "Orissa", "West Bengal");
                                                    foreach ($states as $state) {
                                                        echo "<option value='$state'>$state</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputSkills">Skills</label>
                                            <div class="form-check-inline" name="skills">
                                                <?php
                                                    $skills = array("PHP", "Java", "HTML", "CSS", "Bootstrap", "MYSQL");
                                                    foreach ($skills as $skill) {
                                                        echo "<label class='form-check-label'>
                                                                <input type='checkbox' id='$skill' name='skills[]' class='form-check-input' value='$skill' >$skill
                                                             </label>  &nbsp; &nbsp; &nbsp;";
                                                    }
                                                ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputAge">Age</label>
                                            <input type="number" class="form-control" id="age" name="age" placeholder="Age">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputFile">Resume input</label>
                                            <input type="file" accept="application/pdf" name="resume">
                                            <p class='help-block'>Upload your resume.</p>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputAvatar">Avatar input</label>
                                            <input type="file" accept="image/*" onchange="showUploadImage(event)" name="avatar">
                                            <p class='help-block'>Upload your profile image.</p>
                                        </div>
                                        <button type="submit" id="update-btn"  class="btn btn-dark">Update</button>
                                        <button type="reset" id="cancel-btn" name="cancel"  class="btn btn-warning">Cancel</button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Adding js for profile page -->
    <script src="public/js/profile.js"></script>

</body>
