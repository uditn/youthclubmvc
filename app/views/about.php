<!-- Body part -->

<body>
    <div class="container about-container">
        <button class="accordion">MAIN GOALS :</button>
        <div class="panel">
            <h3>The main goals of organization are :</h3>
            <p>
                * To support youth initiative (help in the process from idea to realization)<br>
                * To increase among youth self-empowerment and self-esteem<br>
                * To involve young people to be an active part of Erasmus+ programme (Youth in
                Action programme) and other local and international funds for youngsters and youth workers.<br>
            </p>
        </div>
        <button class="accordion">MISSION :</button>
        <div class="panel">
            <h3>The missions of organization are :</h3>
            <p>* To support youth formation in active citizenship.<br></p>
        </div>
        <button class="accordion">OBJECTIVES :</button>
        <div class="panel">
            <h3> Main objectives of organisation :</h3>
            <p>
                * To increase among youth sense of initiative and entrepreneurship<br>
                * To enhance employability and improve career aspects<br>
                * To involve youth in local and international projects<br>
                * To support tolerant ways of thinking among young people<br>
                * To support the cooperation in the field of youth work both within Europe and outside, as well as in own country, city, region<br>
                * To facilitate cooperation between young people from different countries<br>
                * To provide different activities for youngster according their interests and needs<br>
            </p>
        </div>
    </div>
    <script src="/public/js/about.js"></script>
</body>