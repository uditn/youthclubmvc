<!DOCTYPE html>
<html lang="en">

<head>
    <title>
        <?php
        if (isset($title)) {
            echo $title;
        }
        ?>
    </title>

    <!--All meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- All Css files -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="/public/css/style.css">

    <!-- All javascript files -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="/public/js/jquery.validate.min.js"></script>
    <script src="public/js/manage-cookies.js"></script>
    <script src="/public/js/jquery.serializeToJSON.js"></script>

</head>

<body>
    <header class="header">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="/"> <img id="brand" src="public/images/brand.jpg" alt="Logo of YouthClub">
                    </a>

                    <a class="navbar-brand brand_name" href="/"><b>YouthClub</b></a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/">Home</a></li>

                        <?php if (
                            $set_header == "welcome" ||
                            $set_header == "profile" ||
                            $set_header == "auth_about" ||
                            $set_header == "changepassword"
                        ) { ?>

                            <li><a href="/profile">Profile</a></li>
                        <?php } ?>

                        <li><a href="#">Our Goal</a></li>
                        <li><a href="#">Our Team</a></li>
                        <li><a href="/about">About</a></li>
                        <li><a href="#">Contact us</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">

                        <?php if (
                            $set_header == "dashboard" ||
                            $set_header == "nonauth_about"
                        ) { ?>
                            <li>
                                <a href="/register">
                                    <span class="glyphicon glyphicon-user"></span> Sign Up
                                </a>
                            </li>

                            <li>
                                <a href="/login">
                                    <span class="glyphicon glyphicon-log-in"></span> Login
                                </a>
                            </li>

                        <?php } else if (
                            $set_header == "welcome" ||
                            $set_header == "profile" ||
                            $set_header == "changepassword" ||
                            $set_header == "auth_about"
                        ) { ?>

                            <li>
                                <a href="/changepassword"> Change Password</a>
                            </li>
                            <li>
                                <a href="/login/logout">
                                    <span class="glyphicon glyphicon-log-out"></span> Log Out
                                </a>
                            </li>

                        <?php } else if ($set_header == "login") { ?>
                            <li>
                                <a href="/register">
                                    <span class="glyphicon glyphicon-user"></span> Sign Up
                                </a>
                            </li>
                        <?php } else if ($set_header == "register") { ?>
                            <li>
                                <a href="/login">
                                    <span class="glyphicon glyphicon-log-in"></span> Login
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </nav>
    </header>