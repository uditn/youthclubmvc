<?php

class App {

    public $config = [];


    function __construct () {

        define("URI", $_SERVER['REQUEST_URI']);
        define("ROOT", $_SERVER['DOCUMENT_ROOT']);

    }

    function autoload () {

        spl_autoload_register(function ($class) {

            $class = strtolower($class);
            if (file_exists(ROOT . '/core/classes/' . $class . '.php')) {

                require_once ROOT . '/core/classes/' . $class . '.php';

            } else if (file_exists(ROOT . '/core/helpers/' . $class . '.php')) {

                require_once ROOT . '/core/helpers/' . $class . '.php';

            }

        });

    }

    function require ($path) {

        require ROOT . $path;

    }


    function config () {

        $this->require('/core/config/Fast2Sms.php');

    }

    function start () {

        session_start();

        if($_COOKIE["token"] != ""){
            $_SESSION["login"] = $_COOKIE["token"];
        } else{
            unset($_SESSION["login"]);
        }

        $route = explode('/', URI);

        $route[1] = strtolower($route[1]);

        if (file_exists(ROOT . '/app/controllers/' . $route[1] . '.php')) {
            $this->require('/app/controllers/' . $route[1] . '.php');
            $controller = new $route[1]($this->config);
        } else {
            $this->require('/app/controllers/main.php');
            $main = new Main();
        }

    }
    
}

?>
